import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

class Database extends AbstractTableModel {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<TableHeader> headers;
    private List<List<TableData>> data;
    
    public Database() {
        headers = new ArrayList<TableHeader>();
        data = new ArrayList<List<TableData>>();
    }
    public void addRow() {
        List<TableData> row = new ArrayList<TableData>();
        for(TableHeader col:headers)
        	if(Ustawienia.czyFabryka == false) // prototyp
        		row.add(col.create2()); // wywołanie metody prototypu
        	else
        		row.add(col.create()); // wywołanie metody fabrykującej
        	
        data.add(row);
        fireTableStructureChanged();
    }
    public void addCol(TableHeader type) {
        headers.add(type);
        for(List<TableData> row:data)
        	if(Ustawienia.czyFabryka == false) // prototyp
        		row.add(type.create2()); // wywołanie metody prototypu
        	else
        		row.add(type.create()); // wywołanie metody fabrykującej
        
        fireTableStructureChanged();
    }

    public int getRowCount() { return data.size(); }
    public int getColumnCount() { return headers.size(); }
    public String getColumnName(int column) {
        return headers.get(column).toString();
    }
    public Object getValueAt(int row, int column) {
        return data.get(row).get(column);
    }
}