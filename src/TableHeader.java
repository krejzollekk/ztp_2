
class TableHeader
{
    private String type;
    protected TableData obiekt = null;
    
    public TableHeader(String type) { this.type = type; }
    public TableHeader(TableData temp, String type){ this.obiekt = temp; this.type = type;}
    public String toString() { return type; }
    
    public TableData create(){ return new TableDataInt(); }
    public TableData create2(){ return (TableData) obiekt.clone(); }
}

class TableHeaderInt extends TableHeader
{
	public TableHeaderInt() {
		super("INT");
	}
    public TableData create(){ 
    	return new TableDataInt();
    }
}

class TableHeaderDouble extends TableHeader
{
	public TableHeaderDouble() {
		super("DOUBLE");
	}
    public TableData create(){ 
    		return new TableDataDouble(); 
    }
}

class TableHeaderChar extends TableHeader
{
	public TableHeaderChar() {
		super("CHAR");
	}
    public TableData create(){ 
    		return new TableDataChar(); 
    }
}

class TableHeaderBoolean extends TableHeader
{
	public TableHeaderBoolean() {
		super("BOOLEAN");
	}
    public TableData create(){ 
    		return new TableDataBoolean(); 
    }
}
