import java.util.Random;

interface TableData {
    final static Random rnd = new Random();
    public Object clone();
}

/*
 * todo:
 * 		int
 * 		double
 * 		char
 * 		boolean
 * 
 */

class TableDataInt implements TableData, Cloneable
{
    private int data;
    public TableDataInt() { data = rnd.nextInt(100); }
    public String toString() { return Integer.toString(data); }
    public Object clone() {
        try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
     }
}

class TableDataDouble implements TableData, Cloneable
{
    private double data;
    public TableDataDouble() { data = rnd.nextDouble(); }
    public String toString() { return Double.toString(data); }
    public Object clone() {
        try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
     }
}

class TableDataChar implements TableData, Cloneable
{
    private char data;
    public TableDataChar() { data = (char)(rnd.nextInt(26) + 'a'); }
    public String toString() { return Character.toString(data); }
    public Object clone() {
        try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
     }
}

class TableDataBoolean implements TableData, Cloneable
{
    private boolean data;
    public TableDataBoolean() { data = rnd.nextBoolean(); }
    public String toString() { return Boolean.toString(data); }
    public Object clone() {
        try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
     }
}


